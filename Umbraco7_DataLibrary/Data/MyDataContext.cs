﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Umbraco7_DataLibrary.Data;

namespace Umbraco7_DataLibrary
{
    public class MyDataContext : DbContext
    {

        public MyDataContext()
            : base("name=db_FirstEntities")
        {
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //throw new UnintentionalCodeFirstException();
        //    base.OnModelCreating(modelBuilder);
        //}

        public DbSet Contacts { get; set; }
        //public virtual DbSet<Contact> Contacts { get; set; }

    }
}