﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco7_DataLibrary.Entities
{
    public class BaseEntity
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
    }
}
