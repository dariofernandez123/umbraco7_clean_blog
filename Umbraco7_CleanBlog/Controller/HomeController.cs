﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Umbraco7_CleanBlog.Controller
{
    public class HomeController : SurfaceController
    {
        //private IAuditService _auditService;

        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Home/";

        private readonly MyAwesomeContext _myAwesome;


        public HomeController(MyAwesomeContext myAwesome)
        {
            _myAwesome = myAwesome;
        }


        public ActionResult RenderFeature()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Featured.cshtml");
        }

        public ActionResult HelloWorld()
        {
            // note: used in Home.cshtml
            //   @{ Html.RenderAction("HelloWorld", "Home"); }
            return Content("<h2>Hello Index Guid " + _myAwesome.MyId +  "</h2>", "text/html");
        }

    }


}