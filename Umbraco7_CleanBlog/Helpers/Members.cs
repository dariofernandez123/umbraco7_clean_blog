﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Umbraco7_CleanBlog.Helpers
{
    public class Members
    {

        private readonly IMemberService _memberService;

        public Members(IMemberService memberService)
        {
            _memberService = memberService;
        }

        public bool EmailAddressExists(string emailAddress)
        {
            IMember member = _memberService.GetByEmail(emailAddress);
            return member != null && member.Email == emailAddress;
        }

        public string SaySomething()
        {
            return "Something...";
           
        }
    }
}