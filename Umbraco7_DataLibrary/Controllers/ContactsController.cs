﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbraco7_DataLibrary.Data;
using Umbraco7_DataLibrary.Interfaces;

namespace Umbraco7_DataLibrary.Controllers
{
    public class ContactsController : Controller
    {
        //private db_FirstEntities db = new db_FirstEntities();

        //private readonly MyDataContext _context;
        //public ContactsController(MyDataContext context)
        //{
        //    _context = context;
        //}

        private readonly IContactRepository _repo;
        public ContactsController(IContactRepository repo)
        {
            _repo = repo;
        }


        // GET: Contacts
        public ActionResult Index()
        {
            //IReadOnlyList<Contact> contacts = _context.Contacts.ToListAsync();
            //return View(db.Contacts.ToList());
            var contacts = _repo.ListAll();
            return View(contacts);
        }



        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Contact contact = db.Contacts.Find(id);
            int iId = (int)id;
            Contact contact = _repo.GetById(iId);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }


        // GET: Contacts/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Name,Email,Message,Attachments")] Contact contact)
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                //db.Contacts.Add(contact);
                //db.SaveChanges();

                _repo.Add(contact);   // repository includes SaveChanges
                return RedirectToAction("Index");
            }

            return View(contact);
        }



        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Contact contact = db.Contacts.Find(id);
            Contact contact = _repo.GetById((int)id);

            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }



        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Email,Message,Attachments")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(contact).State = EntityState.Modified;
                //db.SaveChanges();
                _repo.Update(contact);

                return RedirectToAction("Index");
            }
            return View(contact);
        }



        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Contact contact = db.Contacts.Find(id);
            Contact contact = _repo.GetById((int)id);

            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }



        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Contact contact = db.Contacts.Find(id);
            //db.Contacts.Remove(contact);
            //db.SaveChanges();

            Contact contact = _repo.GetById((int)id);
            _repo.Delete(contact);
            

            return RedirectToAction("Index");
        }


        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
