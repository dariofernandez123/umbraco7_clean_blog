﻿
using System;
using System.Net.Mail;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using Umbraco.Web;
//using Umbraco.Core.Scoping;
using System.Collections.Generic;
using System.IO;
using Umbraco7_CleanBlog.ViewModels;
using Umbraco7_CleanBlog.Models;
using Umbraco.Core.Scoping;

namespace Umbraco7_CleanBlog.Controller
{
    public class ContactSurfaceController : SurfaceController
    {
        //private readonly ISmtpService _smtpService;
        private IScopeProvider _scopeProvider;

        // public ContactSurfaceController(ISmtpService smtpService, IScopeProvider scopeProvider)
        public ContactSurfaceController(IScopeProvider scopeProvider)
        {
            //_smtpService = smtpService;
            _scopeProvider = scopeProvider;
        }

        [HttpGet]
        public ActionResult RenderForm()
        {
            ContactViewModel model = new ContactViewModel() { ContactFormId = CurrentPage.Id };
            return PartialView("~/Views/Partials/Contact/contactForm.cshtml", model);
        }

        [HttpPost]
        public ActionResult RenderForm(ContactViewModel model)
        {
            return PartialView("~/Views/Partials/Contact/contactForm.cshtml", model);
        }

        //public ActionResult SubmitForm(ContactViewModel model, List<HttpPostedFileBase> fileattachments)
        public ActionResult SubmitForm(ContactViewModel model)
        {
            bool success = false;


            if (ModelState.IsValid)
            {

                using (var scope = _scopeProvider.CreateScope())
                {
                    var contactToAdd = new ContactSchema();
                    contactToAdd.Id = model.Id;
                    contactToAdd.Name = model.Name;
                    contactToAdd.Email = model.Email;
                    contactToAdd.Message = model.Message;
                    //contactToAdd.Attachments = model.Attachments;

                    // https://www.aspsnippets.com/Articles/Send-email-with-multiple-attachments-in-ASPNet-MVC.aspx
                    string fileName = "";
                    //IEnumerable<HttpPostedFileBase> fileattachments = model.FilesAttach;
                    //foreach (HttpPostedFileBase attach in fileattachments)
                    //{
                    //    if (attach != null)
                    //    {
                    //        fileName = fileName + ", " + Path.GetFileName(attach.FileName) ;
                    //    }
                    //}

                    // NOTE: NOT WORKING ATTACHING A FILE
                    ////HttpPostedFile fileattached = model.ImageFile;
                    ////fileName = Path.GetFileName(fileattached.FileName);

                    contactToAdd.Attachments = fileName;
                    //scope.Database.Insert<ContactSchema>(contactToAdd);

                    // You must always complete a scope
                    scope.Complete();

                    success = true;
                }

                using (var scope = _scopeProvider.CreateScope())
                {
                    // Scope.Database has what you need/want
                    scope.Database.Fetch<ContactSchema>("Select * From Contacts");
                    // You must always complete a scope
                    scope.Complete();
                }

            }

            //var contactPage = UmbracoContext.Content.GetById(false, model.ContactFormId);
            //var successMessage = contactPage.Value<IHtmlString>("successMessage");
            //var errorMessage = contactPage.Value<IHtmlString>("errorMessage");
            //return PartialView("~/Views/Partials/Contact/result.cshtml", success ? successMessage : errorMessage);

            return PartialView("~/Views/Partials/Contact/result.cshtml", success ? "Success!" : "Failed");
        }



    }
}
