﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Web;

namespace Umbraco7_CleanBlog
{
    public class MyUmbracoStartup : ApplicationEventHandler
    {

        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Example for using Autofac:
            //AutofacStartup.ContainerBuilding += (sender, args) =>
            //{
            //    //add our own services
            //    args.Builder.RegisterControllers(typeof(TestController).Assembly);
            //    args.Builder.RegisterType().As().InstancePerRequest();
            //};
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //// see https://our.umbraco.com/Documentation/Reference/Routing/custom-routes-v7
            //// Custom route to MyProductController which will use a node with a specific ID as the
            //// IPublishedContent for the current rendering page

            //RouteTable.Routes.MapUmbracoRoute(
            //    "ProductCustomRoute",
            //    "Products/{action}/{sku}",
            //    new
            //    {
            //        controller = "MyProduct",
            //        sku = UrlParameter.Optional
            //    },
            //    new ProductsRouteHandler(_productsNodeId));



        }
    }
}