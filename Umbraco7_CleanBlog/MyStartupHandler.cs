﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco7_CleanBlog.Controller;

namespace Umbraco7_CleanBlog
{
    public class MyStartupHandler : ApplicationEventHandler
    {

        #region Methods

        /// <summary>
        /// Application starting.
        /// </summary>
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {

            //TODO: Do some stuff.


            // Boilerplate.
            base.ApplicationStarting(umbracoApplication, applicationContext);

        }


        /// <summary>
        /// Application started.
        /// </summary>
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {

            //TODO: Do some stuff.

            // Boilerplate.
            //base.ApplicationStarted(umbracoApplication, applicationContext);

            var builder = new ContainerBuilder();

            // register all controllers found in this assembly
            builder.RegisterControllers(typeof(MyStartupHandler).Assembly);
            builder.RegisterApiControllers(typeof(MyStartupHandler).Assembly);
            builder.RegisterControllers(typeof(HomeController).Assembly);

            // register Umbraco MVC + web API controllers used by the admin site
            builder.RegisterControllers(typeof(UmbracoApplicationBase).Assembly);
            builder.RegisterApiControllers(typeof(UmbracoApplicationBase).Assembly);

            builder.RegisterApiControllers(typeof(HomeController).Assembly);

            // add custom class to the container as Transient instance
            builder.RegisterType<MyAwesomeContext>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);


        }

        #endregion

    }

}