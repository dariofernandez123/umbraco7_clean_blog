﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco7_CleanBlog.Helpers;

namespace Umbraco7_CleanBlog.Controller
{
    public class TestController : SurfaceController
    {

        private readonly Members _memberHelper;

        public TestController(Members memberHelper)
        {
            _memberHelper = memberHelper;
        }

        //see /umbraco/surface/test/index to see the result
        public ActionResult Index()
        {
            IContentService contentService = Services.ContentService;

            return Content("<h2>Hello " + _memberHelper.SaySomething() + "</h2>", "text/html");
        }
    }

}