﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using Umbraco.Core.Persistence.DatabaseAnnotations;


namespace Umbraco7_CleanBlog.Models
{
    [TableName("Contacts")]
    [PrimaryKey("Id", AutoIncrement = true)]
    [ExplicitColumns]
    public class ContactSchema
    {
        [PrimaryKeyColumn(AutoIncrement = true, IdentitySeed = 1)]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        public string Name { get; set; }

        [Column("Email")]
        public string Email { get; set; }

        [Column("Message")]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string Message { get; set; }

        [Column("Attachments")]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string Attachments { get; set; }

    }
}
