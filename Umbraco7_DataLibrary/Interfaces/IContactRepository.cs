﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco7_DataLibrary.Data;

namespace Umbraco7_DataLibrary.Interfaces
{
    public interface IContactRepository
    {
        Contact GetById(int id);

        IReadOnlyList<Contact> ListAll();

        //Task<T> GetEntityWithSpec(ISpecification<T> spec);

        //Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec);

        //Task<int> CountAsync(ISpecification<T> spec);

        // added methods to change
        void Add(Contact entity);
        void Update(Contact entity);
        void Delete(Contact entity);

        //IEnumerable GetContacts();
    }
} 


