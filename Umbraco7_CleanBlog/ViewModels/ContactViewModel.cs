﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Umbraco7_CleanBlog.ViewModels
{
    public class ContactViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "You must enter a valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your message")]
        [MaxLength(500, ErrorMessage = "Your message must be no longer than 500 characters")]
        public string Message { get; set; }

        public int ContactFormId { get; set; }

        public string Attachments { get; set; }

        public IEnumerable<HttpPostedFileBase> FilesAttach { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
    }
}
