﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Umbraco7_DataLibrary.Data;
using Umbraco7_DataLibrary.Entities;
using Umbraco7_DataLibrary.Interfaces;

namespace Umbraco7_DataLibrary.Repositories
{
    public class ContactRepository : IContactRepository 
    {
        MyDataContext _context = new MyDataContext();

        //private readonly MyDataContext _context;

        //public ContactRepository(MyDataContext context)
        //{
        //    _context = context;
        //}
        //public void Add(Contact entity)
        //{
        //    _context.Set<Contact>().Add(entity);
        //}
        //public void Delete(Contact entity)
        //{
        //    _context.Set<Contact>().Remove(entity);
        //}
        //public void Update(Contact entity)
        //{
        //    _context.Set<Contact>().Attach(entity);
        //    _context.Entry(entity).State = EntityState.Modified;
        //}
        //public Contact GetById(int id)
        //{
        //    return _context.Set<Contact>().Find(id);
        //}
        //public IReadOnlyList<Contact> ListAll()
        //{
        //    return _context.Set<Contact>().ToList();
        //}

        public void Add(Contact entity)
        {
            //_context.Contacts.Add(entity);
            _context.Set<Contact>().Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Contact entity)
        {
            //_context.Contacts.Remove(entity);
            _context.Set<Contact>().Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Contact entity)
        {
            //_context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            _context.Set<Contact>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public Contact GetById(int id)
        {
            return _context.Set<Contact>().Find(id);
        }

        public IReadOnlyList<Contact> ListAll()
        {
            return _context.Set<Contact>().ToList();
        }

        //public void Remove(int Id) { Product p = context.Products.Find(Id); context.Products.Remove(p); context.SaveChanges(); }

    }
}