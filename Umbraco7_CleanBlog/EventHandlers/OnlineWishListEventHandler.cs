﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;
using Autofac.Integration.WebApi;
using Umbraco7_CleanBlog.Helpers;
using Umbraco.Core.Publishing;
using Umbraco.Core.Events;
using Umbraco.Core.Models;

namespace Umbraco7_CleanBlog.EventHandlers
{
    public class OnlineWishListEventHandler : IApplicationEventHandler
    {
        #region IApplicationEventHandler Implementation

        // SEE https://codeshare.co.uk/blog/how-to-start-using-dependency-injection-in-mvc-and-umbraco/

        public void OnApplicationInitialized(UmbracoApplicationBase application, ApplicationContext context)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase application, ApplicationContext context)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase application, ApplicationContext context)
        {
            var builder = new ContainerBuilder();

            // Register all controllers found in this assembly
            builder.RegisterInstance(ApplicationContext.Current).AsSelf();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(typeof(UmbracoApplication).Assembly);// Web API

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // Add types to be resolved
            RegisterTypes(builder, context);

            // Configure Http and Controller Resolvers
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);// web api
            GlobalConfiguration.Configuration.DependencyResolver = resolver; // web api
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }

        #endregion

        #region Private Helpers

        private static void RegisterTypes(ContainerBuilder builder, ApplicationContext applicationContext)
        {
            builder.RegisterInstance(applicationContext.Services.MemberService).As<IMemberService>();

            builder.RegisterType<Members>().AsSelf();

            // add custom class to the container as Transient instance
            builder.RegisterType<MyAwesomeContext>();
        }

 
        #endregion
    }
}

