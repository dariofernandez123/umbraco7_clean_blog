﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Umbraco7_CleanBlog
{
    //implementation of the custom service
    public class ServerInfoService : IServerInfoService
    {
        private readonly HttpRequestBase _umbCtx;

        //requires a request based object so this must be scoped to a request
        public ServerInfoService(HttpRequestBase umbCtx)
        {
            _umbCtx = umbCtx;
        }

        public string GetValue()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Server info!").AppendLine();
            foreach (var key in _umbCtx.ServerVariables.AllKeys)
            {
                sb.AppendLine($"{key} = {_umbCtx.ServerVariables[key]}");
            }
            return sb.ToString();
        }
    }

}