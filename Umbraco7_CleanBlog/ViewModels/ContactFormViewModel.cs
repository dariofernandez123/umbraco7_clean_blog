﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Umbraco7_CleanBlog.ViewModels
{
    public class ContactFormViewModel
    {
        [Required]
        [MaxLength(80, ErrorMessage="Please limit to 80 characters")]
        public string Name { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "Your comments must be no longer than 500 characters")]
        public string Comment { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Your comments must be no longer than 500 characters")]
        public string Subject { get; set; }




    }
}