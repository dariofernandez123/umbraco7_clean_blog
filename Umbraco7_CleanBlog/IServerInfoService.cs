﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbraco7_CleanBlog
{
    //custom service
    public interface IServerInfoService
    {
        string GetValue();
    }

}
