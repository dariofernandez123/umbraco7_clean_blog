﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Umbraco7_CleanBlog.Controller
{
    public class SiteLayoutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/SiteLayout/";

        public ActionResult RenderHeader()
        {
            //List<NavigationListItem> nav = GetObjectFromCache<List<NavigationListItem>>("mainNav", 5, GetNavigationModelFromDatabase);
            ////List<NavigationListItem> nav = GetNavigationModelFromDatabase();
            //return PartialView(PARTIAL_VIEW_FOLDER + "_Header.cshtml", nav);
            return PartialView(PARTIAL_VIEW_FOLDER + "_Header.cshtml");

        }

        public ActionResult RenderIntro()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Intro.cshtml");
        }

        public ActionResult RenderTitleControls()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_TitleControls.cshtml");
        }


        public ActionResult RenderFooter()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Footer.cshtml");
        }
    }

}
