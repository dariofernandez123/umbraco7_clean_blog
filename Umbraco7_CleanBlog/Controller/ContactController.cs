﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco7_CleanBlog.ViewModels;

namespace Umbraco7_CleanBlog.Controller
{
    public class ContactController : SurfaceController
    {
        // GET: Contact
        public ActionResult RenderContactForm()
        {
            var vm = new ContactFormViewModel();
            // /Views/MacroPartials/ContactForm.cshtml
            return PartialView("~/Views/Partials/_ContactForm.cshtml", vm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleContactForm(ContactFormViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Please check the form.");
            }
            //return PartialView("~/Views/Partials/_ContactForm.cshtml", vm);
            return CurrentUmbracoPage(); 
        }
    }
}