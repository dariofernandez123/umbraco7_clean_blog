﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Umbraco7_DataLibrary.Entities;
using Umbraco7_DataLibrary.Interfaces;

namespace Umbraco7_DataLibrary
{

    // this is added as a service so it can be injected (located at API.ApplicationServicesExtensions):
    //    services.AddScoped(typeof(IGenericRepository<>), (typeof(GenericRepository<>)));

    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {

        private readonly MyDataContext _context;

        public GenericRepository(MyDataContext context)
        {
            _context = context;
        }



        //public async Task<T> GetByIdAsync(int id)
        //{
        //    return await _context.Set<T>().FindAsync(id);
        //}

        //public async Task<IReadOnlyList<T>> ListAllAsync()
        //{
        //    return await _context.Set<T>().ToListAsync();
        //}

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public  IReadOnlyList<T> ListAll()
        {
            return  _context.Set<T>().ToList();
        }


        // For the Unit of Work we need to implement these methods to add the entity
        // Add method begins tracking the entity
        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

    }
}