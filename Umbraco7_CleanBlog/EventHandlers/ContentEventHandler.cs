﻿using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;


namespace Umbraco7_CleanBlog.EventHandlers
{
    public class ContentEventHandler : ApplicationEventHandler
    {

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // by DF: this checks when the content (in Umbraco backoffice) is Published

            ContentService.Published += ContentServicePublished;
        }

        private void ContentServicePublished(IPublishingStrategy sender, PublishEventArgs<IContent> args)
        {
            foreach (var node in args.PublishedEntities)
            {

                // by DF: this checks when the Blog content (in Umbraco backoffice) is Published
                //  for example a new blog entry is added or saved  http://cleanblog7.web.local/umbraco#/content/content/edit/1074

                if (node.ContentType.Alias == "blog")
                {
                    //SendMail(node);
                    string s = "test";
                }
            }
        }
    }

}